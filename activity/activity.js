//********OR OPERATOR********//
db.users.find({$or: [{firstName: {$regex: 's', $options: '$i'} }, {lastName: {$regex: 'd', $options: '$i'}}]},
{

    _id: 1,

    firstName: 1,

    lastName: 1

    }
);


//********AND OPERATOR********//
db.users.find( {$and: [{department: "HR"}, {age: {$gte:70}}]} );


//********AND+REGEX+LTE OPERATORS********//
db.users.find( {$and: [{firstName: {$regex:  "e", $options: "$i" }}, {age: {$lte:30}}]} );


